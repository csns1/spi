create procedure mbush_klient_fillim
as
begin
DELETE FROM Klienti
DBCC CHECKIDENT ('KinemaDWH.dbo.Klienti',RESEED, 0)
INSERT into Klienti(total) values ('Total Klient')

update Klienti set totaliId=id 

insert into Klienti (totaliId,total,shteti)
select k.totaliId,k.total,s.emer from KinemaDB.dbo.Shtet as s cross join Klienti k
ORDER BY s.emer

update Klienti set shtetId=id where shteti is not null

INSERT into Klienti (totaliId,total,shtetId,shteti,qyeteti)
select k.totaliId,k.total,k.shtetId,k.shteti,q.emer
from KinemaDB.dbo.Qytet q join Klienti k on q.shtetId=k.shtetId
order by q.shtetId

update Klienti set qyetetId=id where qyeteti is not null

INSERT into Klienti (totaliId,total,shtetId,shteti,qytetId,qyteti,dbo.Klienti.address,addressBurim)
select k.totaliId,k.total,k.shtetId,k.shteti,k.qytetId,k.qyteti,a.rruga,a.id
from KinemaDB.dbo.Address a join Klienti k on a.qytetId=k.qytetId
order by a.qytetid

select * from Klienti;


end
go