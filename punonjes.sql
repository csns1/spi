use DWHMSH
select * from Punonjes
--mbushim nivelit total
insert into Punonjes(total) values ('Total i punjesve')
update punonjes set id_total=id
begin transaction p

--mbushja e nivelit manaxher
insert into Punonjes (id_total,total,emer_manaxher,hiredate,manaxher_burim)
select p.id_total,p.total, FirstName+' '+LastName,e.HireDate,EmployeeID
from Northwind.dbo.Employees e cross join Punonjes p
where EmployeeID in (select ReportsTo from Northwind.dbo.Employees)
update Punonjes set id_manaxher=id where manaxher_burim is not null



--mbushja e nivelit punonjes
insert into Punonjes (id_total,total,id_manaxher,emer_manaxher,hiredate,manaxher_burim,emer_punonjes,birthdate,city,punonjes_burim)
select p.id_total,p.total,p.id_manaxher,p.emer_manaxher,p.hiredate, p.manaxher_burim, e.FirstName+' '+e.LastName,e.BirthDate,e.City,e.EmployeeID
from Northwind.dbo.Employees e  join Punonjes p on e.ReportsTo=p.manaxher_burim

update Punonjes set id_punonjes=id where punonjes_burim is not null

commit transaction p