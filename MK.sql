
CREATE TABLE [dbo].[Shtet](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[emer] [nvarchar](50) NOT NULL,
	[pershkrim] [nvarchar](50) NULL
) 
CREATE TABLE [dbo].[Qytet] (
[id] int NOT NULL IDENTITY(1,1) ,
[emer] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
[pershkrim] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
[shtet_id] int NOT NULL ,
CONSTRAINT [PK__Qytet__3213E83FAC719CD2] PRIMARY KEY ([id]),
CONSTRAINT [shtet_qytet_fk] FOREIGN KEY ([shtet_id]) REFERENCES [dbo].[Shtet] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
)
CREATE TABLE [dbo].[Adresa] (
[id] int NOT NULL IDENTITY(1,1) ,
[rruga] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
[kodi_postar] int NULL ,
[qytet_id] int NOT NULL ,
CONSTRAINT [PK__Adresa__3213E83FF298C202] PRIMARY KEY ([id]),
CONSTRAINT [address_qytet] FOREIGN KEY ([qytet_id]) REFERENCES [dbo].[Qytet] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
)
CREATE TABLE dbo.Bank(
id int NOT NULL IDENTITY(1,1) ,
bankName varchar(255) NOT NULL ,
tel varchar(255) NULL ,
email varchar(255) NULL ,
PRIMARY KEY (id),
)
CREATE TABLE dbo.Dege(
id int NOT NULL IDENTITY(1,1) ,
pershkrim varchar(255) NOT NULL ,
bank_id int NOT NULL ,
adres_id int NOT NULL ,
CONSTRAINT bank_deg FOREIGN KEY (bank_id) REFERENCES dbo.Bank (id),
CONSTRAINT adrese_deg FOREIGN KEY (adres_id) REFERENCES dbo.Adresa (id),
PRIMARY KEY (id)
)
CREATE TABLE dbo.Klient(
id int NOT NULL IDENTITY(1,1) ,
emer varchar(255) NOT NULL ,
mbiemer varchar(255) NOT NULL ,
ditelindja DATE NOT NULL ,
gjinia char(1) CHECK 'M' OR 'F',
email varchar(50) NOT NULL ,
tel varchar(50) NOT NULL ,
pozicioni varchar(50) NOT NULL ,
adres_id int NOT NULL ,
dege_id int NOT NULL ,
CONSTRAINT klient_deg FOREIGN KEY (dege_id) REFERENCES dbo.Dege (id),
CONSTRAINT adrese_klient FOREIGN KEY (adres_id) REFERENCES dbo.Adresa (id),
PRIMARY KEY (id)
)
CREATE TABLE [dbo].[Kredi] (
[id] int NOT NULL IDENTITY(1,1) ,
[shuma] money NOT NULL ,
[tipi] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
[data_mbylljes] datetime NOT NULL ,
[data_fillimit] datetime NOT NULL ,
[statusi] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
[pagesa_mujore] money NOT NULL ,
[klient_id] int NOT NULL ,
CONSTRAINT [PK__Kredi__3213E83FA2A3DF40] PRIMARY KEY ([id]),
CONSTRAINT [klient_kredi] FOREIGN KEY ([klient_id]) REFERENCES [dbo].[Klient] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
)
CREATE TABLE dbo.Tip_Transaksioni(
id int NOT NULL IDENTITY(1,1) ,
pershkrim varchar(255) NOT NULL ,
PRIMARY KEY (id)
)
CREATE TABLE dbo.Llogari(
id int NOT NULL IDENTITY(1,1) ,
balance money NOT NULL ,
monedha varchar(20) NOT NULL,
data_hapjes datetime NOT NULL ,
data_mbylljes datetime NULL ,
kredi_id int NULL,
klient_id int NOT NULL,
tip_llogarie_id int NOT NULL,
status_llogarie_id int NOT NULL,
CONSTRAINT klient_llogari FOREIGN KEY (klient_id) REFERENCES dbo.Klient (id),
CONSTRAINT kredi_llogari FOREIGN KEY (kredi_id) REFERENCES dbo.Kredi (id),
CONSTRAINT tip_llogarie_llogari FOREIGN KEY (tip_llogarie_id) REFERENCES dbo.Tip_Llogarie (id),
CONSTRAINT status_llogarie_llogari FOREIGN KEY (status_llogarie_id) REFERENCES dbo.Status_Llogarie (id),
PRIMARY KEY (id)
)
CREATE TABLE dbo.Transaksion(
id int NOT NULL IDENTITY(1,1) ,
data datetime NOT NULL ,
shuma money NOT NULL,
perfitues_id  int NOT NULL,
klient_id int NOT NULL,
tip_trans_id int NOT NULL ,
CONSTRAINT klient_trans FOREIGN KEY (klient_id) REFERENCES dbo.Klient (id),
CONSTRAINT llog_trans FOREIGN KEY (perfitues_id) REFERENCES dbo.Llogari (id),
CONSTRAINT tip_trans_trans FOREIGN KEY (tip_trans_id) REFERENCES dbo.Tip_Transaksioni (id),
PRIMARY KEY (id)
)
CREATE TABLE [dbo].[Status_Llogarie] (
[id] int NOT NULL IDENTITY(1,1) ,
[pershkrim] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
CONSTRAINT [PK__Status_L__3213E83F429E483C] PRIMARY KEY ([id])
)
CREATE TABLE [dbo].[Tip_Llogarie] (
[id] int NOT NULL IDENTITY(1,1) ,
[pershkrim] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
[interest] float(53) NOT NULL ,
CONSTRAINT [PK__Tip_Llog__3213E83F2074955B] PRIMARY KEY ([id])
)
create table koha_dim(
id_k int primary key identity(1,1),
id_t int null,
total varchar(50) null,
id_v int null,
vit int null,
id_m int null,
muaj int null,
persh_m varchar(50) null,
burim int null
)


create table klient_dim(
id_c int primary key identity(1,1),
id_t int null,
total varchar(50) null,
id_klient int null,
emri varchar(30) null,
mbiemri varchar(30) null,
dtlindje date null,
tel varchar(30) null,
id_d int null,
pershk_d varchar (50)null,
banka varchar (50) null,
klient_burim int null
)
create table llogari_dim(
id_l int primary key identity(1,1),
id_t int null,
total varchar(50) null,
id_tip int null,
tipi varchar(30) null,
interesi int null,
id_11 int null,
balanca int null,
monedha varchar(7) null,
dt_hapje datetime null,
dt_mbyllje datetime null,
llogari_burim varchar(50) null
)
create table transaksiontip_dim(
id_tr int primary key identity(1,1),
id_t int null,
total varchar(50) null,
id_tip_tr int null,
tipi varchar(30) null,
transaksiontip_burim int null
)
create table fakti(
id int primary key identity(1,1),
koha int null FOREIGN KEY REFERENCES dbo.koha_dim(id_k),
klienti int null FOREIGN KEY REFERENCES dbo.klient_dim(id_c),
llogari int null FOREIGN KEY REFERENCES dbo.llogari_dim(id_l),
transaksion int null FOREIGN KEY REFERENCES dbo.transaksiontip_dim(id_tr),
shuma_transaksion money null,
nr_transaksione int null
)
CREATE PROCEDURE [dbo].mbush_koha  
AS
BEGIN
insert into koha_dim (total) values ('Total')
insert into koha_dim (id_t,total, vit)
select k.id_t,k.total,t.viti from (
select distinct year(data) viti
from BankDB.dbo.Transaksion) as t cross join koha_dim k
order by t.viti
update koha_dim set id_t=id_k where id_v is null
update koha_dim set id_v=id_k where vit is not null
insert into koha_dim (id_t,total,id_v,vit,muaj,persh_m, burim)
select k.id_t,k.total,k.id_v,k.vit,t.muaji,
case t.muaji 
when 1 then 'Janar'
when 2 then 'Shkurt'
when 3 then 'Mars'
when 4 then 'Prill'
when 5 then 'Maj'
when 6 then 'Qershor'
when 7 then 'Korrik'
when 8 then 'Gusht'
when 9 then 'Shtator'
when 10 then 'Tetor'
when 11 then 'Néntor'
when 12 then 'Dhjetor'
end,
CAST(t.viti as nvarchar)+cast(t.muaji as nvarchar)
from (
select distinct year(data) viti, MONTH(data) muaji
from BankDB.dbo.Transaksion
) as t join koha_dim k on t.viti=k.vit
order by t.viti
update koha_dim set id_m=id_k where muaj is not null
END

---------NEXT------
insert into klient_dim (total) values ('Total')
insert into klient_dim(id_t,total, pershk_d, banka)
select k.id_t,k.total,b.per, 'National Bank'
from
klient_dim k cross join (
select distinct pershkrim per from BankDB.dbo.Dege
) as b
select * from dbo.klient_dim
update klient_dim set id_t=id_c where id_d is null
update klient_dim set id_d=id_c where pershk_d is not null
insert into klient_dim (id_t, total, pershk_d, banka, emri,mbiemri, tel, dtlindje,klient_burim)
select k.id_t,k.total,k.pershk_d,k.banka,cu.emer,cu.mbiemer,cu.tel,cu.ditelindja,cu.id
from BankDB.dbo.Klient cu join BankDB.dbo.Dege b on cu.dege_id=b.id
join klient_dim k on b.pershkrim=k.pershk_d
update klient_dim set id_klient=id_c where emri is not null
-----NEXT-----
insert into llogari_dim (total) values ('Total')
update llogari_dim set id_t=id_l where id_tip is NULL

insert into llogari_dim(id_t,total, tipi, interesi)
select k.id_t,k.total,a.per,a.inter
from
llogari_dim k cross join (
select distinct pershkrim per,interest inter from BankDB.dbo.Tip_Llogarie
) as a
select * from dbo.llogari_dim
update llogari_dim set id_tip=id_l where tipi is not null

insert into llogari_dim( id_t,total,tipi,interesi,balanca,monedha,dt_hapje,dt_mbyllje,llogari_burim)
SELECT DISTINCT k.id_t,k.total,k.tipi,k.interesi,balanca,a.monedha,data_hapjes,data_mbylljes,a.id
from BankDB.dbo.Llogari a join BankDB.dbo.Tip_Llogarie at on a.tip_llogarie_id=at.id
join llogari_dim k on at.pershkrim=k.tipi
update llogari_dim set llogari_dim.id_11=id_l where dt_hapje is not null
----NEXT-----
insert into transaksiontip_dim(total) values('Total')

insert into transaksiontip_dim(id_t,total,tipi,transaksiontip_burim)
select k.id_t,k.total,b.pershkrim,b.id
FROM
transaksiontip_dim k cross join (
select pershkrim , id from BankDB.dbo.Tip_Transaksioni tr) as b

update transaksiontip_dim set id_t=id_tr where id_tip_tr is NULL
update transaksiontip_dim set id_tip_tr=id_tr where tipi is not NULL

---NEXT-----
insert into fakti
SELECT k.id_k,kl.id_c,ll.id_l,tr.id_tr,sum(trans.shuma),count(trans.id)
from BankDB.dbo.Tip_Transaksioni tra join BankDB.dbo.Transaksion trans on tra.id=trans.tip_trans_id
join BankDB.dbo.Llogari acc on trans.perfitues_id=acc.id
join BankDB.dbo.Klient cu on acc.klient_id=cu.id join klient_dim kl on cu.id=klient_burim
join llogari_dim ll on acc.id=ll.llogari_burim
join transaksiontip_dim tr on tra.id=tr.transaksiontip_burim
join koha_dim k on CAST(year(trans.data) as nvarchar)+CAST(MONTH(trans.data) as nvarchar)=k.burim
GROUP BY k.id_k,kl.id_c,ll.id_l,tr.id_tr



---REPORTS----
select muaj,vit,avg(shuma_transaksion) mesatare from fakti f join koha_dim ko on
f.koha=ko.id_k
group by muaj,vit

----NEXT-----
select distinct emri,mbiemri,dtlindje,tel,banka,klient_burim
from klient_dim k join fakti f on k.id_c=f.klienti
where f.shuma_transaksion =(select max(f1.shuma_transaksion) from fakti f1)
-----NEXT ------
select tipi,count(id_tip_tr) numri
transaksiontip_dim t join fakti on t.id_tr=f.transaksion
where tipi='Terheqje' or tipi='Depozitim'
group by tipi

