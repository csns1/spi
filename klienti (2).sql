use DWHMSH
select * from Klienti
--mbushim nivelit total
insert into Klienti(total) values ('Total i klienteve')
update Klienti set id_total=id
begin transaction p

--mbushja e nivelit shteti
insert into Klienti (id_total,total,shteti)
select distinct  k.id_total,k.total,Country
from Northwind.dbo.Customers c cross join Klienti k
update Klienti set id_shteti=id where shteti is not null


--mbushja e nivelit qyteti
insert into Klienti(id_total,total,id_shteti,shteti,qyteti)
select distinct k.id_total,k.total,k.id_shteti,k.shteti,c.City
from Northwind.dbo.Customers c  join klienti k on c.Country=k.shteti
order by k.shteti,c.City

update Klienti set id_qyteti=id where qyteti is not null


--mbushja e nivelit klient
insert into Klienti(id_total,total,id_shteti,shteti,id_qyteti,qyteti,klienti,kontakt,fax,klienti_burim)
select k.id_total,k.total,k.id_shteti,k.shteti,k.id_qyteti,k.qyteti,c.CompanyName,c.ContactName,
c.Fax,c.CustomerID
from Northwind.dbo.Customers c  join klienti k on c.City=k.qyteti
update Klienti set id_klienti=id where klienti is not null

commit transaction p