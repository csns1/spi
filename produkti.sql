use DWHMSH
select * from Produkti
--mbushim nivelit total
insert into Produkti(total) values ('Total i produkteve')
update Produkti set id_total=id
begin transaction p

--mbushja e nivelit kategori
insert into Produkti (id_total,total,kategori,kategori_pershkrim,kategori_burim)
select   p.id_total,p.total,c.CategoryName,c.Description,c.CategoryID
from Northwind.dbo.Categories c cross join Produkti p
order by CategoryName
update Produkti set id_kategori=id where kategori_burim is not null


--mbushja e nivelit furnitori
insert into Produkti (id_total,total,furnitori,adrese,phone,qyteti,furnitori_burim)
select   p.id_total,p.total,s.CompanyName,s.Address,s.Phone,s.City,s.SupplierID
from Northwind.dbo.Suppliers s cross join (select * from Produkti where id_kategori is null) as p
order by s.CompanyName
update Produkti set id_furnitori=id where furnitori_burim is not null


--mbushja e nivelit produkt
insert into Produkti(id_total,total,id_kategori,kategori,kategori_pershkrim,kategori_burim,
	id_furnitori,furnitori,adrese,phone,qyteti,furnitori_burim,produkt,produkt_burim)
select prs.id_total,prs.total,prc.id_kategori,prc.kategori,prc.kategori_pershkrim,prc.kategori_burim,
	prs.id_furnitori,prs.furnitori,prs.adrese,prs.phone,prs.qyteti,prs.furnitori_burim,
	p.ProductName,p.ProductID
from Northwind.dbo.products p  join Produkti prc on p.CategoryID=prc.kategori_burim
join Produkti prs on p.SupplierID=prs.furnitori_burim

update Produkti set id_produkt=id where produkt_burim is not null

commit transaction p