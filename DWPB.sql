CREATE TABLE dbo.Kategori (
id int NOT NULL IDENTITY(1,1) ,
kategori varchar(255) NOT NULL ,
PRIMARY KEY (id)
)

CREATE TABLE dbo.Shtet (
id int NOT NULL IDENTITY(1,1) ,
emer varchar(255) UNIQUE NOT NULL ,
PRIMARY KEY (id)
)

CREATE TABLE dbo.Qytet (
id int NOT NULL IDENTITY(1,1) ,
emer varchar(255) NOT NULL ,
shtetId int NOT NULL ,
PRIMARY KEY (id),
CONSTRAINT shtet_qytet_fk FOREIGN KEY (shtetId) REFERENCES dbo.Shtet (id)
)

CREATE TABLE dbo.Address (
id int NOT NULL IDENTITY(1,1) ,
rruga varchar(255) NULL ,
qytetId int NULL ,
PRIMARY KEY (id),
CONSTRAINT address_qytet FOREIGN KEY (qytetId) REFERENCES dbo.Qytet (id)
)

CREATE TABLE dbo.StudioFilmi (
id int NOT NULL IDENTITY(1,1) ,
emri varchar(255) NOT NULL ,
addressId int NOT NULL ,
PRIMARY KEY (id),
CONSTRAINT address_studio FOREIGN KEY (addressId) REFERENCES dbo.Address (id)
)

CREATE TABLE dbo.Film (
id int NOT NULL IDENTITY(1,1) ,
titulli varchar(255) NOT NULL ,
pershkrimi varchar(255) NULL ,
vleresimi varchar(255) NULL,
kategoriId int NOT NULL ,
studioFilmiId int NOT NULL ,
PRIMARY KEY (id),
CONSTRAINT kategori_film FOREIGN KEY (kategoriId) REFERENCES dbo.Kategori (id),
CONSTRAINT studio_film FOREIGN KEY (studioFilmiId) REFERENCES dbo.StudioFilmi (id)
)

CREATE TABLE dbo.TipKlienti (
id int NOT NULL IDENTITY(1,1) ,
emer varchar(255) NOT NULL ,
ulje int NOT NULL ,
PRIMARY KEY (id),
CONSTRAINT chkulje CHECK (ulje<=30)
)

CREATE TABLE dbo.Klient (
id int NOT NULL IDENTITY(1,1) ,
emer varchar(255) NOT NULL ,
mbiemer varchar(255) NOT NULL ,
email varchar(255) NULL ,
addressId int NOT NULL ,
tipId int NOT NULL,
PRIMARY KEY (id),
CONSTRAINT addreess_klient FOREIGN KEY (addressId) REFERENCES dbo.Address (id),
CONSTRAINT tip_klient FOREIGN KEY (tipId) REFERENCES  dbo.TipKlienti(id)
)

CREATE TABLE dbo.Salle (
id int NOT NULL IDENTITY(1,1) ,
emer varchar(255) NOT NULL ,
addressId int NOT NULL ,
PRIMARY KEY (id),
CONSTRAINT kinema_salle FOREIGN KEY (kinemaId) REFERENCES dbo.Kinema (id)
)

CREATE TABLE dbo.Shfaqe (
id int NOT NULL IDENTITY(1,1) ,
data timestamp NOT NULL ,
filmId int NOT NULL ,
salleId int NOT NULL ,
PRIMARY KEY (id),
CONSTRAINT shfaqe_film FOREIGN KEY (filmId) REFERENCES dbo.Film (id),
CONSTRAINT shfaqe_salle FOREIGN KEY (salleId) REFERENCES dbo.Salle (id)
)

CREATE TABLE dbo.Kinema (
id int NOT NULL IDENTITY(1,1) ,
emer varchar(255) NOT NULL ,
addressId int NOT NULL ,
PRIMARY KEY (id),
CONSTRAINT address_kinema FOREIGN KEY (addressId) REFERENCES dbo.Address (id)
)

CREATE TABLE dbo.Fatura (
id int NOT NULL IDENTITY(1,1) ,
data datetime NOT NULL ,
pagesaTotale money NOT NULL ,
klientId int NOT NULL ,
PRIMARY KEY (id),
CONSTRAINT klient_fature FOREIGN KEY (klientId) REFERENCES dbo.Klient (id)
)

CREATE TABLE dbo.Bileta (
id int NOT NULL  IDENTITY(1,1),
cmimiPerBilete money NOT NULL ,
sasia int NOT NULL ,
shfaqeId int NOT NULL ,
fatureId int NOT NULL ,
PRIMARY KEY (id),
CONSTRAINT fature_bilete FOREIGN KEY (fatureId) REFERENCES dbo.Fatura (id),
CONSTRAINT shfaqe_bilete FOREIGN KEY (shfaqeId) REFERENCES dbo.Shfaqe (id)
)

CREATE TABLE dbo.Koha (
id int NOT NULL IDENTITY(1,1),
totaliId int  NULL ,
total VARCHAR(255)  NULL ,
vitiId int NULL,
viti int NULL,
muajiId int NULL ,
muaji int NULL ,
pershkrim nvarchar(15)  NULL ,
PRIMARY KEY (id))

CREATE TABLE dbo.Klienti (
id int NOT NULL IDENTITY(1,1),
totaliId int  NULL ,
total VARCHAR(255)  NULL ,
tipId int NULL,
tipEmer varchar(255) NULL,
tipBurim int NULL,
klientId int NULL,
emer VARCHAR(255) NULL,
mbiemer VARCHAR(255) NULL,
email VARCHAR(255) NULL,
klientBurim int NULL,
PRIMARY KEY (id))

CREATE TABLE dbo.Salle (
id int NOT NULL IDENTITY(1,1),
totaliId int  NULL ,
total VARCHAR(255)  NULL ,
shtetId int NULL,
shteti VARCHAR(255) NULL,
shtetBurim int NULL,
qytetId int NULL ,
qyteti VARCHAR(255) NULL ,
qytetBurim int NULL,
addressId int NULL,
address VARCHAR(255) NULL,
addressBurim int NULL,
kinemaId int NULL,
emerKinema VARCHAR(255) NULL,
kinemaBurim int NULL,
salleId int NULL,
emerSalle VARCHAR(255) NULL,
salleBurim int NULL,
PRIMARY KEY (id))

CREATE TABLE dbo.Film (
id int NOT NULL IDENTITY(1,1),
totaliId int  NULL ,
total VARCHAR(255)  NULL ,
studioFilmiId int NULL,
studioEmer VARCHAR(255) NULL,
studioBurim int NULL,
kategoriId int NULL,
kategoriEmer VARCHAR(255) NULL,
kategoriBurim int NULL,
filmId int NULL,
filmEmer VARCHAR(255) NULL,
vleresimi	int NULL,
pershkrimi VARCHAR(255) NULL,
filmBurim int NULL,
PRIMARY KEY (id))

CREATE TABLE dbo.Shitje (
id int NOT NULL IDENTITY(1,1) ,
koha int NOT NULL ,
klienti int NOT NULL ,
film int NOT NULL,
salle int NOT NULL ,
sasi int NOT NULL ,
shuma money NOT  NULL,
PRIMARY KEY (id),
CONSTRAINT koha_fakt FOREIGN KEY (koha) REFERENCES dbo.Koha (id),
CONSTRAINT film_fakt FOREIGN KEY (film) REFERENCES dbo.Film (id),
CONSTRAINT salle_fakt FOREIGN KEY (salle) REFERENCES dbo.Salle (id),
CONSTRAINT klienti_fakt FOREIGN KEY (klienti) REFERENCES dbo.Klienti (id)
)