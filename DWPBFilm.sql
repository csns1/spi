create procedure mbush_film_fillim
as
begin
DELETE FROM Film
DBCC CHECKIDENT ('KinemaDWH.dbo.Film',RESEED, 0)
INSERT into Film(total) values ('Total Film')

update Film set totaliId=id 

insert into Film (totaliId,total,kategoriEmer,kategoriBurim)
select f.totaliId,f.total,kat.kategori,kat.id from KinemaDB.dbo.Kategori as kat cross join Film f
ORDER BY kat.kategori

update Film set kategoriId=id where kategoriEmer is not null

insert into Film (totaliId,total,studioEmer,studioBurim)
select f.totaliId,f.total,std.emri,std.id from KinemaDB.dbo.StudioFilmi as std cross join (select * from Film where kategoriId is null) f
ORDER BY std.emri

update Film set studioFilmiId=id where studioEmer is not null

insert into Film (totaliId,total,kategoriId,kategoriEmer,kategoriBurim,
studioFilmiId,studioEmer,studioBurim,filmEmer,pershkrimi,vleresimi,filmBurim)
select f1.totaliId,f1.total,f1.kategoriId,f1.kategoriEmer,f1.kategoriBurim,
f2.studioFilmiId,f2.studioEmer,f2.studioBurim,og.titulli,og.pershkrimi,og.vleresimi,og.id
from KinemaDB.dbo.Film og  join Film f1 on f1.kategoriBurim=og.kategoriId
join Film f2 on f2.studioBurim=og.studioFilmiId

update Film set filmId=id where filmBurim is not null


select * from Film;


end
go