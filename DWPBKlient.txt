create procedure mbush_klient_fillim
as
begin

INSERT into Klienti(total) values ('Total Klient')

update Klienti set totaliId=id 

insert into Klienti (totaliId,total,shteti,shtetBurim)
select k.totaliId,k.total,s.emer,s.id from KinemaDB.dbo.Shtet as s cross join Klienti k
ORDER BY s.emer

update Klienti set shtetId=id where shteti is not null

INSERT into Klienti (totaliId,total,shtetId,shteti,shtetBurim,qyteti,qytetBurim)
select k.totaliId,k.total,k.shtetId,k.shteti,k.shtetBurim,q.emer,q.id
from KinemaDB.dbo.Qytet q join Klienti k on q.shtetId=k.shtetBurim
order by q.shtetId

update Klienti set qytetId=id where qyteti is not null

INSERT into Klienti (totaliId,total,shtetId,shteti,shtetBurim,qytetId,qyteti,qytetBurim,address,addressBurim)
select k.totaliId,k.total,k.shtetId,k.shteti,k.shtetBurim,k.qytetId,k.qyteti,k.qytetBurim,a.rruga,a.id
from KinemaDB.dbo.Address a join Klienti k on a.qytetId=k.qytetBurim
order by a.qytetid

update Klienti set addressId=id where address is not null

INSERT into Klienti (totaliId,total,shtetId,shteti,shtetBurim,qytetId,qyteti,qytetBurim,address,addressBurim,emer,mbiemer,email,klientBurim)
select k.totaliId,k.total,k.shtetId,k.shteti,k.shtetBurim,k.qytetId,k.qyteti,k.qytetBurim,k.address,k.addressBurim,og.emer,og.mbiemer,og.email,og.id
from KinemaDB.dbo.Klient og join Klienti k on og.addressId=k.addressBurim
order by og.addressId

update Klienti set klientId=id where emer is not null


select * from Klienti;


end
go