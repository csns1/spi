--exec mbush_klient_fillim

create procedure mbush_klient_fillim
as
begin

--mbushja e nivelit total
insert into Klienti (Totali) values ('Totali i klienteve')
update Klienti set id_total=id

--mbushja e nivelit Shtet

insert into Klienti (id_total,Totali,Shteti)
select  distinct k.id_total,k.Totali,isnull(c.Country,'I panjohur')
from Northwind.dbo.Customers C cross join Klienti K

update Klienti set id_shtet=id where Shteti is not null

--mbushja e nivelit qytet
insert into Klienti(id_total,Totali,id_shtet,Shteti,Qyteti)
select distinct k.id_total,k.Totali,k.id_shtet,k.Shteti,
isnull(c.City,'I Panjohur') 
from Klienti K join Northwind.dbo.Customers c
on k.Shteti=isnull(c.Country,'I panjohur')

update Klienti set id_qytet=id where Qyteti is not null

--Mbushja e nivelit klient
insert into Klienti(id_total,Totali,id_shtet,Shteti,id_qytet,Qyteti,
	Emer,Kontakti,Telefon,Klient_burim)
select k.id_total,k.Totali,k.id_shtet,k.Shteti,k.id_qytet,k.Qyteti,
	c.CompanyName,c.ContactName,c.Phone,c.CustomerID
from Klienti k join Northwind.dbo.Customers c 
on (k.Qyteti=c.City and k.Shteti=c.Country)
order by k.Shteti,k.Qyteti,c.CompanyName

update Klienti set id_klient=id where Klient_burim is not null

end
go