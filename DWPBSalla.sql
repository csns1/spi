create procedure mbush_salle_fillim
as
begin
DELETE FROM Salle
DBCC CHECKIDENT ('KinemaDWH.dbo.Salle',RESEED, 0)
INSERT into Salle(total) values ('Total Salle')

update Salle set totaliId=id 

insert into Salle (totaliId,total,shteti,shtetBurim)
select s.totaliId,s.total,sh.emer,sh.id from KinemaDB.dbo.Shtet as sh cross join Salle s
ORDER BY sh.emer

update Salle set shtetId=id where shteti is not null

INSERT into Salle (totaliId,total,shtetId,shteti,shtetBurim,qyteti,qytetBurim)
select s.totaliId,s.total,s.shtetId,s.shteti,s.shtetBurim,q.emer,q.id
from KinemaDB.dbo.Qytet q join Salle s on q.shtetId=s.shtetBurim
order by q.shtetId

update Salle set qytetId=id where qyteti is not null

INSERT into Salle (totaliId,total,shtetId,shteti,shtetBurim,qytetId,qyteti,qytetBurim,address,addressBurim)
select s.totaliId,s.total,s.shtetId,s.shteti,s.shtetBurim,s.qytetId,s.qyteti,s.qytetBurim,a.rruga,a.id
from KinemaDB.dbo.Address a join Salle s on a.qytetId=s.qytetBurim
order by a.qytetid

update Salle set addressId=id where address is not null

INSERT into Salle (totaliId,total,shtetId,shteti,shtetBurim,qytetId,qyteti,qytetBurim,addressId,address,addressBurim,emerKinema,kinemaBurim)
select s.totaliId,s.total,s.shtetId,s.shteti,s.shtetBurim,s.qytetId,s.qyteti,s.qytetBurim,s.addressId,s.address,s.addressBurim,k.emer,k.id
from KinemaDB.dbo.Kinema k join Salle s on k.addressId=s.addressBurim
order by k.addressId

update Salle set kinemaId=id where emerKinema is not null

INSERT into Salle (totaliId,total,shtetId,shteti,shtetBurim,qytetId,qyteti,qytetBurim,addressId,address,addressBurim,kinemaId,emerKinema,kinemaBurim,emerSalle,salleBurim)
select s.totaliId,s.total,s.shtetId,s.shteti,s.shtetBurim,s.qytetId,s.qyteti,s.qytetBurim,s.addressId,s.address,s.addressBurim,s.kinemaId,s.emerKinema,s.kinemaBurim,og.emer,og.id
from KinemaDB.dbo.Salle og join Salle s on og.kinemaId=s.kinemaBurim
order by og.kinemaId

update Salle set salleId=id where emerSalle is not null

select * from Salle;


end
go