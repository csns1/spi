create procedure mbush_koha_fillim
as
begin

INSERT into koha (total) values ('Total Koha');

update koha set totaliId=id ;

insert into koha (totaliId,total,viti)
select k.totaliId,k.total,f.viti from(
select distinct year(data) viti
from KinemaDB.dbo.Fatura ) as f cross join koha k
ORDER BY f.viti;

update koha set vitiId=id where viti is not null;

INSERT into koha (totaliId,total,vitiId,viti,muaji,pershkrim)
select k.totaliId,k.total,k.vitiId,k.viti,f.muaji,dbo.merrMuaj(f.muaji)
from ( select distinct year(data) viti ,month(data) muaji from KinemaDB.dbo.Fatura) as f join koha k on k.viti=f.viti
order by f.viti;

update koha set muajiId=id where muaji is not null;

select* from koha;

end
go



----NEXT-----
create procedure mbush_klient_fillim
as
begin

INSERT into Klienti(total) values ('Total Klient')

update Klienti set totaliId=id 

insert into Klienti (totaliId,total,shteti,shtetBurim)
select k.totaliId,k.total,s.emer,s.id from KinemaDB.dbo.Shtet as s cross join Klienti k
ORDER BY s.emer

update Klienti set shtetId=id where shteti is not null

INSERT into Klienti (totaliId,total,shtetId,shteti,shtetBurim,qyteti,qytetBurim)
select k.totaliId,k.total,k.shtetId,k.shteti,k.shtetBurim,q.emer,q.id
from KinemaDB.dbo.Qytet q join Klienti k on q.shtetId=k.shtetBurim
order by q.shtetId

update Klienti set qytetId=id where qyteti is not null

INSERT into Klienti (totaliId,total,shtetId,shteti,shtetBurim,qytetId,qyteti,qytetBurim,address,addressBurim)
select k.totaliId,k.total,k.shtetId,k.shteti,k.shtetBurim,k.qytetId,k.qyteti,k.qytetBurim,a.rruga,a.id
from KinemaDB.dbo.Address a join Klienti k on a.qytetId=k.qytetBurim
order by a.qytetid

update Klienti set addressId=id where address is not null

INSERT into Klienti (totaliId,total,shtetId,shteti,shtetBurim,qytetId,qyteti,qytetBurim,address,addressBurim,emer,mbiemer,email,klientBurim)
select k.totaliId,k.total,k.shtetId,k.shteti,k.shtetBurim,k.qytetId,k.qyteti,k.qytetBurim,k.address,k.addressBurim,og.emer,og.mbiemer,og.email,og.id
from KinemaDB.dbo.Klient og join Klienti k on og.addressId=k.addressBurim
order by og.addressId

update Klienti set klientId=id where emer is not null


select * from Klienti;


end
go

----NEXT-----
create procedure mbush_film_fillim
as
begin
DELETE FROM Film
DBCC CHECKIDENT ('KinemaDWH.dbo.Film',RESEED, 0)
INSERT into Film(total) values ('Total Film')

update Film set totaliId=id 

insert into Film (totaliId,total,kategoriEmer,kategoriBurim)
select f.totaliId,f.total,kat.kategori,kat.id from KinemaDB.dbo.Kategori as kat cross join Film f
ORDER BY kat.kategori

update Film set kategoriId=id where kategoriEmer is not null

insert into Film (totaliId,total,studioEmer,studioBurim)
select f.totaliId,f.total,std.emri,std.id from KinemaDB.dbo.StudioFilmi as std cross join (select * from Film where kategoriId is null) f
ORDER BY std.emri

update Film set studioFilmiId=id where studioEmer is not null

insert into Film (totaliId,total,kategoriId,kategoriEmer,kategoriBurim,
studioFilmiId,studioEmer,studioBurim,filmEmer,pershkrimi,vleresimi,filmBurim)
select f1.totaliId,f1.total,f1.kategoriId,f1.kategoriEmer,f1.kategoriBurim,
f2.studioFilmiId,f2.studioEmer,f2.studioBurim,og.titulli,og.pershkrimi,og.vleresimi,og.id
from KinemaDB.dbo.Film og  join Film f1 on f1.kategoriBurim=og.kategoriId
join Film f2 on f2.studioBurim=og.studioFilmiId

update Film set filmId=id where filmBurim is not null


select * from Film;


end
go


------NEXT-----
create procedure mbush_salle_fillim
as
begin
DELETE FROM Salle
DBCC CHECKIDENT ('KinemaDWH.dbo.Salle',RESEED, 0)
INSERT into Salle(total) values ('Total Salle')

update Salle set totaliId=id 

insert into Salle (totaliId,total,shteti,shtetBurim)
select s.totaliId,s.total,sh.emer,sh.id from KinemaDB.dbo.Shtet as sh cross join Salle s
ORDER BY sh.emer

update Salle set shtetId=id where shteti is not null

INSERT into Salle (totaliId,total,shtetId,shteti,shtetBurim,qyteti,qytetBurim)
select s.totaliId,s.total,s.shtetId,s.shteti,s.shtetBurim,q.emer,q.id
from KinemaDB.dbo.Qytet q join Salle s on q.shtetId=s.shtetBurim
order by q.shtetId

update Salle set qytetId=id where qyteti is not null

INSERT into Salle (totaliId,total,shtetId,shteti,shtetBurim,qytetId,qyteti,qytetBurim,address,addressBurim)
select s.totaliId,s.total,s.shtetId,s.shteti,s.shtetBurim,s.qytetId,s.qyteti,s.qytetBurim,a.rruga,a.id
from KinemaDB.dbo.Address a join Salle s on a.qytetId=s.qytetBurim
order by a.qytetid

update Salle set addressId=id where address is not null

INSERT into Salle (totaliId,total,shtetId,shteti,shtetBurim,qytetId,qyteti,qytetBurim,addressId,address,addressBurim,emerKinema,kinemaBurim)
select s.totaliId,s.total,s.shtetId,s.shteti,s.shtetBurim,s.qytetId,s.qyteti,s.qytetBurim,s.addressId,s.address,s.addressBurim,k.emer,k.id
from KinemaDB.dbo.Kinema k join Salle s on k.addressId=s.addressBurim
order by k.addressId

update Salle set kinemaId=id where emerKinema is not null

INSERT into Salle (totaliId,total,shtetId,shteti,shtetBurim,qytetId,qyteti,qytetBurim,addressId,address,addressBurim,kinemaId,emerKinema,kinemaBurim,emerSalle,salleBurim)
select s.totaliId,s.total,s.shtetId,s.shteti,s.shtetBurim,s.qytetId,s.qyteti,s.qytetBurim,s.addressId,s.address,s.addressBurim,s.kinemaId,s.emerKinema,s.kinemaBurim,og.emer,og.id
from KinemaDB.dbo.Salle og join Salle s on og.kinemaId=s.kinemaBurim
order by og.kinemaId

update Salle set salleId=id where emerSalle is not null

select * from Salle;


end
go