select * from shitje

insert into shitje(produkt,klient,kohe,punonjes,sasia,vlera)
select dp.id produkt,dk.id klient,dko.id koha,dpun.id opunonjes,sum(od.quantity),sum(od.quantity*od.unitprice)
from Northwind.dbo.Products p join Northwind.dbo.[Order Details] od on p.ProductID=od.ProductID
	join Northwind.dbo.Orders o on od.OrderID=o.OrderID
	join Northwind.dbo.Employees e on o.EmployeeID=e.EmployeeID
	join Northwind.dbo.Customers c on o.CustomerID=c.CustomerID
	join Produkti dp on dp.produkt_burim=p.ProductID
	join Punonjes dpun on dpun.punonjes_burim=e.EmployeeID
	join Klienti dk on dk.klienti_burim=c.CustomerID
	join Koha dko on dko.lidhje=cast(year(o.OrderDate) as nvarchar)+cast(month(o.OrderDate) as nvarchar)
group by dp.id,dk.id,dko.id,dpun.id

insert into shitje(produkt,klient,kohe,punonjes,sasia,vlera)
select dp.id produkt,dk.id klient,dko.id koha,dpun.id opunonjes,
sum(od.quantity),sum(od.quantity*od.unitprice)
from  Northwind.dbo.[Order Details] od 
	join Northwind.dbo.Orders o on od.OrderID=o.OrderID
	join Produkti dp on dp.produkt_burim=od.ProductID
	join Punonjes dpun on dpun.punonjes_burim=o.EmployeeID
	join Klienti dk on dk.klienti_burim=o.CustomerID
	join Koha dko on dko.lidhje=cast(year(o.OrderDate) as nvarchar)+cast(month(o.OrderDate) as nvarchar)
group by dp.id,dk.id,dko.id,dpun.id