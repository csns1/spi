#include <fstream>
#include <iostream>
#include <tchar.h>

using namespace std;

int main(int argc,  char** argv)
{   string path=argv[1];
    int start=atoi(argv[2]);
    int end=atoi(argv[3]);
    int freq[128];
    for (int k = 0; k < 128; k++)
    {
        freq[k] = 0;
    }
    for(int i=start;i<=end;i++){
        ifstream inFile;
        char ch;
        inFile.open(path+to_string(i)+".txt");
        if (!inFile)
        {
            cout << "Skedari nuk mund te hapet."<<endl;
            return 1;
        }
        ch = inFile.get();
        while (ch != EOF)
        {   ch = toupper(ch);
            freq[ch]++;
            ch = inFile.get();
        }
    }
    ofstream resultFile;
    int fileNumber=end/(end-start+1);
    resultFile.open(path+"result"+to_string(fileNumber)+".txt");
    for (char ch = 'A'; ch <= 'Z'; ch++)
    {
        resultFile << ch << " " << freq[ch] << "\n";
    }
    return 0;
}