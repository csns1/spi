#include <windows.h>
#include <cstdio>
#include <tchar.h>
#include <string>
#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>

using namespace std;

int main( int argc, TCHAR *argv[] ) {
    std::string path = argv[1];
    int numberOfProcesses=atoi(argv[2]);
    if(100%numberOfProcesses!=0){
        cout<<"Error NUMRI I PROCESEVE DUHET TE JETE PLOTPJESTUES I 100";
        return 0;
    }
    STARTUPINFO si[numberOfProcesses];
    PROCESS_INFORMATION pi[numberOfProcesses];

    for(int i=0;i<100;i=i+(100/numberOfProcesses)){
        int last=i+(100/numberOfProcesses);
        int first=i+1;
        cout<<"["<<first<<";"<<last<<"]"<<endl;
        ZeroMemory( &si, sizeof(si) );
        si[i].cb = sizeof(si);
        ZeroMemory( &pi, sizeof(pi) );
        string s=path+"child.exe "+path+" "+to_string(first)+" "+to_string(last);
        char cstr[s.size() + 1];
        strcpy(cstr, s.c_str());
        // Start the child process.
        if(CreateProcess( nullptr,
                            cstr,        // Command line
                            nullptr,
                            nullptr,
                            FALSE,          // Set handle inheritance to FALSE
                            0,              // No creation flags
                            nullptr,
                            nullptr,
                            &si[i],
                            &pi[i] )
                )
        {
            WaitForSingleObject(pi[i].hProcess,INFINITE);
            CloseHandle( pi[i].hProcess );
            CloseHandle( pi[i].hThread );

        }
    }
    cout<<endl;
    int freq[128];
    char sorted[127];
    for (int k = 0; k < 128; k++)
    {
        freq[k] = 0;
        sorted[k]=k;
    }
    for(int i=1;i<=numberOfProcesses;i++){
        ifstream resFile;
        resFile.open(path+"result"+to_string(i)+".txt");
        int number;
        char c;
        while (resFile >> c >> number)
        {
            freq[c]+=number;
        }
     }
    for (char ch = 'A'; ch <= 'Z'; ch++)
    {    for (char ch2 = ch+1; ch2 <='Z'; ch2++) {
            if (freq[ch2] > freq[ch]) {
                char tmpc=sorted[ch];
                int fr=freq[ch];
                sorted[ch] = sorted[ch2];
                sorted[ch2]=tmpc;

                freq[ch] = freq[ch2];
                freq[ch2] = fr;
            }
        }
    }
    for(char ch = 'A'; ch <= 'Z'; ch++){
        cout<<sorted[ch]<<" "<<freq[ch]<<endl;
    }

    return 0;
}