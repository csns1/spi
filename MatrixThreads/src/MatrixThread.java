public class MatrixThread extends Thread {
    private int[] array;
    private int sum;
    public MatrixThread(int[] array){
        this.array=array;
        sum=0;
    }
    public void run(){
    for (int i:array)sum+=i;
    }
    public int getSum(){
        return sum;
    }
}
