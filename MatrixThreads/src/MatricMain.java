import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
public class MatricMain {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Numri Rreshtave");

        int matrixRow = scan.nextInt();

        System.out.println("Numri Kolonave");

        int matrixCol = scan.nextInt();

        //defining 2D array to hold matrix data
        int[][] matrix = new int[matrixRow][matrixCol];
        // Enter Matrix Data
        enterMatrixData(scan, matrix, matrixRow, matrixCol);
        List<MatrixThread> threads=new ArrayList();
        for(int i=0;i<matrixRow;i++){
            MatrixThread mt=new MatrixThread(matrix[i]);
            threads.add(mt);
            mt.start();
        }
        boolean isProcessing;
        do {
            isProcessing = false;
            for (MatrixThread t : threads) {
                if (t.isAlive()) {
                    isProcessing = true;
                    break;
                }
            }
        } while (isProcessing);
        int max=-99999;
        for(MatrixThread thread:threads){
            if(thread.getSum()>max)max=thread.getSum();
        }
        System.out.println(max);
    }
    public static void enterMatrixData(Scanner scan, int[][] matrix, int matrixRow, int matrixCol){
        System.out.println("Vendos Te Dhenat");

        for (int i = 0; i < matrixRow; i++)
        {
            for (int j = 0; j < matrixCol; j++)
            {
                matrix[i][j] = scan.nextInt();
            }
        }
    }

    public static void printMatrix(int[][] matrix, int matrixRow, int matrixCol){
        System.out.println("Matrica : ");

        for (int i = 0; i < matrixRow; i++)
        {
            for (int j = 0; j < matrixCol; j++)
            {
                System.out.print(matrix[i][j]+"\t");
            }

            System.out.println();
        }
    }
}