create procedure mbush_koha_fillim
as
begin

INSERT into koha (total) values ('Total Koha');

update koha set totaliId=id ;

insert into koha (totaliId,total,viti)
select k.totaliId,k.total,f.viti from(
select distinct year(data) viti
from KinemaDB.dbo.Fatura ) as f cross join koha k
ORDER BY f.viti;

update koha set vitiId=id where viti is not null;

INSERT into koha (totaliId,total,vitiId,viti,muaji,pershkrim)
select k.totaliId,k.total,k.vitiId,k.viti,f.muaji,dbo.merrMuaj(f.muaji)
from ( select distinct year(data) viti ,month(data) muaji from KinemaDB.dbo.Fatura) as f join koha k on k.viti=f.viti
order by f.viti;

update koha set muajiId=id where muaji is not null;

select* from koha;

end
go