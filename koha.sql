use DWHMSH
select * from koha
--mbushim nivelit total
insert into koha(pershkrim_total) values ('Total i kohes')
update koha set id_total=id

--mbushja e nivelit vit
declare @vit int
set @vit=1995
while @vit<=1999
begin
insert into koha(id_total,pershkrim_total,vit,pershkrim_vit)
values (1,'Total i kohes',@vit,'Nje mije e nenteqind e nentedhjete e pese')

set @vit=@vit+1
end
update koha set id_vit=id where vit is not null

//mbushja e nivelit muaj
declare @vit int
declare @muaj int
set @vit=1995
while @vit<=1999
begin
	set @muaj=1
	while @muaj<=12
	begin
		insert into koha(vit,muaj,muaj_pershkrim)
		values (@vit,@muaj,'Muaji '+cast(@muaj as nvarchar))
		set @muaj=@muaj+1
	end
	set @vit=@vit+1
end


update muaj
set 
	muaj.id_total=vit.id_total,
	muaj.pershkrim_total=vit.pershkrim_total,
	muaj.id_vit=vit.id_vit,
	muaj.pershkrim_vit=vit.pershkrim_vit,
	muaj.id_muaj=muaj.id,
	muaj.lidhje=cast(muaj.vit as nvarchar)+cast(muaj.muaj as char(2))
from koha as muaj join koha as vit
on (muaj.vit=vit.vit and muaj.muaj is not null and vit.muaj is null)